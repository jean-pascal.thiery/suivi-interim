package com.gitlab.jeanpascal.thiery.suiviinterim.business

import java.time.Clock
import java.util.Date
import java.util.UUID

data class Adresse(
    val street: String,
    val zipCode: String,
    val city: String
)

data class Employeur(
    val name: String,
    val adresse: Adresse
)

data class Etablissement(
    val name: String,
    val adresse: Adresse
)

typealias Service = String

data class PeriodeDeMission(
    val debut: Date,
    val fin: Date
)

data class Mission(
    val par: Employeur,
    val etablissement: Etablissement,
    val service: Service,
    val periodeDeMission: PeriodeDeMission,
    val tauxHoraire: Double
)

data class InterimMissionID(val id: String) : StreamId {
    companion object {
        fun new(): InterimMissionID = InterimMissionID(UUID.randomUUID().toString())
    }
}


//  --  Commands

sealed class InterimMissionCommand : Command {
    abstract val id: InterimMissionID
    override fun id(): StreamId = id
}

data class ProposerMission(
    override val id: InterimMissionID,
    val par: Employeur,
    val etablissement: Etablissement,
    val service: Service,
    val periodeDeMission: PeriodeDeMission,
    val tauxHoraire: Double
) : InterimMissionCommand()

data class RejectMission(
    override val id: InterimMissionID,
    val reason: String
) : InterimMissionCommand()

data class AcceptMission(
    override val id: InterimMissionID
) : InterimMissionCommand()

data class StartMission(
    override val id: InterimMissionID
) : InterimMissionCommand()

data class FinishMission(
    override val id: InterimMissionID
) : InterimMissionCommand()

//  --  Events

sealed class InterimMissionEvent(clock: Clock = Clock.systemUTC()) : Event {
    abstract val id: InterimMissionID
    private val happenedDate: Long = clock.millis()
    override fun id(): InterimMissionID = id
    override fun happenedDate(): Long = happenedDate
}

data class InterimMissionProposed(
    override val id: InterimMissionID,
    val mission: Mission
) : InterimMissionEvent()

data class InterimMissionRejected(
    override val id: InterimMissionID,
    val reason: String
) : InterimMissionEvent()

data class InterimMissionAccepted(
    override val id: InterimMissionID
) : InterimMissionEvent()

data class InterimMissionStarted(
    override val id: InterimMissionID
) : InterimMissionEvent()

data class InterimMissionFinished(
    override val id: InterimMissionID
) : InterimMissionEvent()

//  -- States

sealed class InterimMissionState : State {
    abstract val id: InterimMissionID
    override fun id(): StreamId = id
}

object InterimMissionNotExist : InterimMissionState() {
    override val id: InterimMissionID
        get() = InterimMissionID("Unknown")
}

data class InterimMissionValidating(
    override val id: InterimMissionID,
    val mission: Mission
) : InterimMissionState()

data class InterimMissionAborted(
    override val id: InterimMissionID,
    val reason: String
) : InterimMissionState()

data class InterimMissionReady(
    override val id: InterimMissionID,
    val mission: Mission
) : InterimMissionState()

data class InterimMissionInProgress(
    override val id: InterimMissionID,
    val mission: Mission
) : InterimMissionState()

data class InterimMissionEnding(
    override val id: InterimMissionID,
    val mission: Mission
) : InterimMissionState()

