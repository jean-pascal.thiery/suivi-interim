package com.gitlab.jeanpascal.thiery.suiviinterim.business

import arrow.core.Either

typealias DecideResult = Either<String, List<InterimMissionEvent>>

class SuiviInterim : Aggregate<InterimMissionCommand, InterimMissionState, InterimMissionEvent> {

    override fun decide(
        command: InterimMissionCommand,
        state: InterimMissionState
    ): DecideResult = when (state) {
        InterimMissionNotExist -> decideOnNotExistingMission(command)
        is InterimMissionValidating -> decodeOnInterimMissionValidating(command, state)
        is InterimMissionReady -> decodeOnInterimMissionReady(command, state)
        is InterimMissionInProgress -> decodeOnInterimMissionInProgress(command, state)
        else -> Either.Left("Unable to decide on state ${state::class.java.simpleName}")
    }

    private fun decideOnNotExistingMission(command: InterimMissionCommand): DecideResult = when (command) {
        is ProposerMission -> Either.Right(
            listOf(
                InterimMissionProposed(
                    command.id,
                    Mission(
                        command.par,
                        command.etablissement,
                        command.service,
                        command.periodeDeMission,
                        command.tauxHoraire
                    )
                )
            )
        )
        else -> Either.Left("Command ${command::class.java.simpleName} is available when mission not exist")
    }

    private fun decodeOnInterimMissionValidating(
        command: InterimMissionCommand,
        state: InterimMissionValidating
    ): DecideResult = when (command) {
        is AcceptMission -> Either.Right(
            listOf(
                InterimMissionAccepted(
                    state.id
                )
            )
        )
        is RejectMission -> Either.Right(
            listOf(
                InterimMissionRejected(
                    state.id,
                    command.reason
                )
            )
        )
        else -> Either.Left("Command ${command::class.java.simpleName} is not available when mission is validating")
    }

    private fun decodeOnInterimMissionReady(
        command: InterimMissionCommand,
        state: InterimMissionReady
    ): DecideResult = when (command) {
        is RejectMission -> Either.Right(
            listOf(
                InterimMissionRejected(
                    state.id,
                    command.reason
                )
            )
        )
        is StartMission -> Either.Right(
            listOf(
                InterimMissionStarted(
                    state.id
                )
            )
        )
        else -> Either.Left("Command ${command::class.java.simpleName} is not available when mission is ready")
    }


    private fun decodeOnInterimMissionInProgress(
        command: InterimMissionCommand,
        state: InterimMissionInProgress
    ): DecideResult = when (command) {
        is FinishMission -> Either.Right(
            listOf(
                InterimMissionFinished(
                    state.id
                )
            )
        )
        else -> Either.Left("Command ${command::class.java.simpleName} is not available when mission is in progress")
    }

    override fun apply(state: InterimMissionState, event: InterimMissionEvent): InterimMissionState =
        when (state) {
            InterimMissionNotExist -> applyOnNotExist(event)
            is InterimMissionValidating -> applyOnInterimMissionValidating(state, event)
            is InterimMissionReady -> applyOnInterimMissionReady(state, event)
            is InterimMissionInProgress -> applyOnInterimMissionInProgress(state, event)
            else -> state
        }

    private fun applyOnNotExist(event: InterimMissionEvent): InterimMissionState = when (event) {
        is InterimMissionProposed -> InterimMissionValidating(
            event.id,
            event.mission
        )
        else -> InterimMissionNotExist
    }

    private fun applyOnInterimMissionValidating(
        state: InterimMissionValidating,
        event: InterimMissionEvent
    ): InterimMissionState =
        when (event) {
            is InterimMissionProposed -> state
            is InterimMissionAccepted -> InterimMissionReady(event.id, state.mission)
            is InterimMissionRejected -> InterimMissionAborted(
                event.id,
                event.reason
            )
            else -> state
        }

    private fun applyOnInterimMissionReady(
        state: InterimMissionReady,
        event: InterimMissionEvent
    ): InterimMissionState = when (event) {
        is InterimMissionRejected -> InterimMissionAborted(
            state.id,
            event.reason
        )
        else -> state
    }

    private fun applyOnInterimMissionInProgress(
        state: InterimMissionInProgress,
        event: InterimMissionEvent
    ): InterimMissionState = when (event) {
        is InterimMissionFinished -> InterimMissionEnding(
            state.id,
            state.mission
        )
        else -> state
    }

    override fun notExistState(): InterimMissionState = InterimMissionNotExist

    override fun getEventType(): Class<InterimMissionEvent> = InterimMissionEvent::class.java
}