package com.gitlab.jeanpascal.thiery.suiviinterim.business

enum class BusAddresses(val address: String) {

    DOMAIN_COMMAND("domain.command"),
    DOMAIN_EVENT("domain.event"),
    APPLICATION_ERROR("application.error"),
    MISSION_DETAIL("mission.detail"),
    ETABLISSEMENT_STATS("etablissement.stats")

}