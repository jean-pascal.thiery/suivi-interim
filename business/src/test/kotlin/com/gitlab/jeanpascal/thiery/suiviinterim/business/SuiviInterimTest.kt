package com.gitlab.jeanpascal.thiery.suiviinterim.business

import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import java.time.Instant
import java.util.*

val defaultid = InterimMissionID("1234")

val defaultEmployeur = Employeur(
    "Amene",
    Adresse("24 faubourg des blés", "911", "Policea")
)

val defaultEtablissement = Etablissement(
    "Largely bio tech",
    Adresse("3 boulevard des allongées", "105 Cedex", "Avallon")
)

val defaultPeriod = PeriodeDeMission(
    Date.from(Instant.parse("2022-12-03T00:00:00.00Z")),
    Date.from(Instant.parse("2022-12-04T00:00:00.00Z"))
)

val defaultMission = Mission(
    defaultEmployeur,
    defaultEtablissement,
    "Labo",
    defaultPeriod,
    22.0
)

class SuiviInterimTest {

    @TestFactory
    fun `Decide on SuiviInterim`() = listOf(
        decideTestOnSuiviInterimWith()
            .given(InterimMissionNotExist)
            .whenApplyCommand(
                ProposerMission(
                    defaultid,
                    defaultEmployeur,
                    defaultEtablissement,
                    "Laboratoire d'analyse",
                    defaultPeriod,
                    22.0
                )
            )
            .then(
                SuccessDecideResultExpected.commandSucceeded(InterimMissionProposed::class)
            ),
        decideTestOnSuiviInterimWith()
            .given(
                InterimMissionValidating(
                    defaultid,
                    defaultMission
                )
            )
            .whenApplyCommand(
                RejectMission(
                    defaultid,
                    "Trop loin"
                )
            )
            .then(
                SuccessDecideResultExpected.commandSucceeded(InterimMissionRejected::class)
            ),
        decideTestOnSuiviInterimWith()
            .given(
                InterimMissionValidating(
                    defaultid,
                    defaultMission
                )
            )
            .whenApplyCommand(
                AcceptMission(
                    defaultid
                )
            )
            .then(SuccessDecideResultExpected.commandSucceeded(InterimMissionAccepted::class)),
        decideTestOnSuiviInterimWith()
            .given(
                InterimMissionReady(
                    defaultid,
                    defaultMission
                )
            )
            .whenApplyCommand(
                RejectMission(
                    defaultid,
                    "J'ai un imprevu"
                )
            )
            .then(SuccessDecideResultExpected.commandSucceeded(InterimMissionRejected::class)),
        decideTestOnSuiviInterimWith()
            .given(
                InterimMissionReady(
                    defaultid,
                    defaultMission
                )
            )
            .whenApplyCommand(
                StartMission(defaultid)
            )
            .then(
                SuccessDecideResultExpected.commandSucceeded(InterimMissionStarted::class)
            ),
        decideTestOnSuiviInterimWith()
            .given(
                InterimMissionInProgress(
                    defaultid,
                    defaultMission
                )
            )
            .whenApplyCommand(
                FinishMission(defaultid)
            )
            .then(
                SuccessDecideResultExpected.commandSucceeded(InterimMissionFinished::class)
            )
    ).map {
        DynamicTest.dynamicTest(
            "When ${it.command::class.simpleName} on change request state ${it.initialState::class.simpleName} then expecting ${it.expectedResult}",
            assertOnDecideFixture(it, SuiviInterim())
        )
    }

    @TestFactory
    fun `Apply on SuiviInterim`() = listOf(
        applyTestOnSuiviInterimWith()
            .given(InterimMissionNotExist)
            .whenApplyEvent(
                InterimMissionProposed(
                    defaultid,
                    defaultMission
                )
            )
            .then(
                InterimMissionValidating(
                    defaultid,
                    defaultMission
                )
            ),
        applyTestOnSuiviInterimWith()
            .given(
                InterimMissionValidating(
                    defaultid,
                    defaultMission
                )
            )
            .whenApplyEvent(
                InterimMissionRejected(
                    defaultid,
                    "Trop loin"
                )
            )
            .then(
                InterimMissionAborted(
                    defaultid,
                    "Trop loin"
                )
            ),
        applyTestOnSuiviInterimWith()
            .given(
                InterimMissionValidating(
                    defaultid,
                    defaultMission
                )
            )
            .whenApplyEvent(
                InterimMissionAccepted(
                    defaultid
                )
            )
            .then(
                InterimMissionReady(
                    defaultid,
                    defaultMission
                )
            ),
        applyTestOnSuiviInterimWith()
            .given(
                InterimMissionReady(
                defaultid,
                defaultMission
            )
            )
            .whenApplyEvent(
                InterimMissionRejected(
                    defaultid,
                    "J'ai un imprevu"
                )
            )
            .then(
                InterimMissionAborted(
                    defaultid,
                    "J'ai un imprevu"
                )
            ),
        applyTestOnSuiviInterimWith()
            .given(
                InterimMissionInProgress(
                defaultid,
                defaultMission
            )
            )
            .whenApplyEvent(
                InterimMissionFinished(
                    defaultid
                )
            )
            .then(
                InterimMissionEnding(
                defaultid,
                defaultMission
            )
            )
    ).map {
        DynamicTest.dynamicTest(
            "Given project state ${it.initialState::class.simpleName}, When applying event ${it.eventToApply}, Then expecting to get state ${it.expectedState::class.simpleName}",
            assertOnApply(it, SuiviInterim())
        )
    }
}

//  Alias
fun decideTestOnSuiviInterimWith(): DecideStateAppender<InterimMissionCommand, InterimMissionState, InterimMissionEvent> =
    decideTestWith()

fun applyTestOnSuiviInterimWith(): ApplierStateAppender<InterimMissionState, InterimMissionEvent> = applierTestWith()