package com.gitlab.jeanpascal.thiery.suiviinterim.command.infra.api

import com.gitlab.jeanpascal.thiery.suiviinterim.business.*
import io.quarkus.runtime.annotations.RegisterForReflection
import io.smallrye.mutiny.Uni
import io.vertx.mutiny.core.Vertx
import java.time.Instant
import java.util.*
import javax.enterprise.inject.Default
import javax.inject.Inject
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.*
import kotlin.properties.Delegates

@Path("/mission")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
class MissionCommandHttpEntrypoint {

    @Inject
    @field: Default
    lateinit var vertx: Vertx

    @POST
    fun missionProposed(requestDto: MissionDto, @Context uriInfo: UriInfo): Uni<Response> {

        if (requestDto.tauxHoraire < 0) {
            return Uni.createFrom().item(
                Response.status(Response.Status.BAD_REQUEST).build()
            )
        }

        val missionID = InterimMissionID.new()
        return vertx.eventBus()
            .request<HandleCommandResult>(
                "domain.command",
                ProposerMission(
                    missionID,
                    Employeur(
                        requestDto.employeur.name,
                        requestDto.employeur.address.convert(),
                    ),
                    Etablissement(
                        requestDto.etablissement.name,
                        requestDto.etablissement.address.convert()
                    ),
                    requestDto.service,
                    requestDto.period.convert(),
                    requestDto.tauxHoraire
                )
            )
            .map {
                it.body()
            }
            .map {
                when (it) {
                    is SuccessfullyHandleCommand<*, *> -> {
                        val uriBuilder: UriBuilder = uriInfo.absolutePathBuilder.path("/")
                        uriBuilder.path(missionID.id)
                        Response.accepted(it).location(uriBuilder.build()).build()
                    }
                    is FailedToHandleCommand<*> -> Response.status(Response.Status.BAD_REQUEST).entity(it.reason)
                        .build()
                    is NoopToHandleCommand<*> -> Response.status(Response.Status.NOT_MODIFIED).build()
                }
            }
    }

}

@RegisterForReflection
class PeriodDto {
    lateinit var start: String
    lateinit var end: String

    fun convert(): PeriodeDeMission = PeriodeDeMission(
        Date.from(
            Instant.parse(
                start
            )
        ),
        Date.from(
            Instant.parse(
                end
            )
        )
    )
}

@RegisterForReflection
class AdresseDto {
    lateinit var street: String
    lateinit var zipCode: String
    lateinit var city: String

    fun convert(): Adresse = Adresse(
        street,
        zipCode,
        city
    )
}

@RegisterForReflection
class EmployeurDto {
    lateinit var name: String
    lateinit var address: AdresseDto
}

@RegisterForReflection
class EtablissementDto {
    lateinit var name: String
    lateinit var address: AdresseDto
}

@RegisterForReflection
class MissionDto {
    lateinit var employeur: EmployeurDto
    lateinit var etablissement: EtablissementDto
    lateinit var service: String
    lateinit var period: PeriodDto
    var tauxHoraire by Delegates.notNull<Double>()
}
