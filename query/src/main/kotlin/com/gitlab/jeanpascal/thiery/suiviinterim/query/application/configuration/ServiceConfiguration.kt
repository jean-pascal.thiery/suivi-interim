package com.gitlab.jeanpascal.thiery.suiviinterim.query.application.configuration

import com.gitlab.jeanpascal.thiery.suiviinterim.query.infra.DomainBusCodec
import com.gitlab.jeanpascal.thiery.suiviinterim.query.infra.spi.*
import io.quarkus.runtime.StartupEvent
import io.vertx.core.Vertx
import org.slf4j.LoggerFactory
import javax.enterprise.context.Dependent
import javax.enterprise.event.Observes

@Dependent
class ServiceConfiguration {

    val logger = LoggerFactory.getLogger(ServiceConfiguration::class.java)

    var codecRegistered = false

    fun startMissionDetailRepository(
        @Observes startupEvent: StartupEvent,
        vertx: Vertx,
        missionDetailRepository: MissionDetailRepository
    ) {
        vertx.deployVerticle(missionDetailRepository)
    }

    fun startEtablissementRepository(
        @Observes startupEvent: StartupEvent,
        vertx: Vertx,
        etalissementRepository: EtalissementRepository
    ) {
        vertx.deployVerticle(etalissementRepository)
    }

    fun startVertx(@Observes startupEvent: StartupEvent, vertx: Vertx) {
        val eventBus = vertx.eventBus()

        if (!codecRegistered) {
            eventBus.registerDefaultCodec(
                MissionDetails::class.java,
                DomainBusCodec(MissionDetails::class.java)
            )
            eventBus.registerDefaultCodec(
                EtablissementStat::class.java,
                DomainBusCodec(EtablissementStat::class.java)
            )
            eventBus.registerDefaultCodec(
                EtablissementStats::class.java,
                DomainBusCodec(EtablissementStats::class.java)
            )
            codecRegistered = true
        }
    }

}