package com.gitlab.jeanpascal.thiery.suiviinterim.query.infra.spi

import com.gitlab.jeanpascal.thiery.suiviinterim.business.*
import io.vertx.core.AbstractVerticle
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped

typealias EtablissementName = String

@ApplicationScoped
class EtalissementRepository : AbstractVerticle() {

    private val logger = LoggerFactory.getLogger(EtalissementRepository::class.java)

    private val missionToEtablissement = mutableMapOf<InterimMissionID, EtablissementName>()
    private val cache = mutableMapOf<EtablissementName, EtablissementStat>()

    override fun start() {
        logger.info("Starting")
        vertx.eventBus()
            .consumer<InterimMissionEvent>(BusAddresses.DOMAIN_EVENT.address) { message ->
                logger.debug("Received event ${message.body()}")
                when (val event = message.body()) {
                    is InterimMissionProposed -> {
                        val etablissement = event.mission.etablissement
                        if (!missionToEtablissement.containsKey(event.id)) {
                            missionToEtablissement[event.id] = etablissement.name
                        }
                        if (!cache.containsKey(etablissement.name)) {
                            cache[etablissement.name] = EtablissementStat(
                                etablissement
                            )
                        }
                    }
                    is InterimMissionFinished -> {
                        missionToEtablissement[event.id]?.let { etablissementName ->
                            cache[etablissementName]?.let { current ->
                                cache[etablissementName] = current.copy(
                                    nBMission = current.nBMission + 1
                                )
                            }
                        }
                    }
                    else -> logger.debug("Drop event $event")
                }
            }
        vertx.eventBus()
            .consumer<Any>(BusAddresses.ETABLISSEMENT_STATS.address) { message ->
                message.reply(EtablissementStats(cache.values.toList()))
                logger.debug("Finish processing ${message.body()}")
            }
    }
}

data class EtablissementStats(
    val etablissementStats: List<EtablissementStat>
)

data class EtablissementStat(
    val etablissement: Etablissement,
    val nBMission: Int = 0
)