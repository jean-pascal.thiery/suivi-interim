package com.gitlab.jeanpascal.thiery.suiviinterim.query.infra.api

import com.gitlab.jeanpascal.thiery.suiviinterim.business.BusAddresses
import com.gitlab.jeanpascal.thiery.suiviinterim.query.infra.spi.EtablissementStat
import com.gitlab.jeanpascal.thiery.suiviinterim.query.infra.spi.EtablissementStats
import io.smallrye.mutiny.Uni
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.mutiny.core.eventbus.EventBus
import org.slf4j.LoggerFactory
import javax.enterprise.inject.Default
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/etablissements")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
class EtablissementQueryEntrypoint {

    @Inject
    @field: Default
    lateinit var eventBus: EventBus

    private val logger = LoggerFactory.getLogger(EtablissementQueryEntrypoint::class.java)

    @GET
    fun getEtalissements(): Uni<Response> {
        return safetyProcessResponseFromEventBus<Any, EtablissementStats>(
            eventBus,
            BusAddresses.ETABLISSEMENT_STATS.address,
            ""
        ) { Response.status(Response.Status.OK).entity(toJson(it.body().etablissementStats)).build() }
    }

    private fun toJson(etablissementStats: List<EtablissementStat>): JsonArray {
        val res = JsonArray()
        etablissementStats.forEach { etablissementStat ->
            val root = JsonObject()
            root.put("name", etablissementStat.etablissement.name)
            val address = etablissementStat.etablissement.adresse
            val addressJson = JsonObject()
            addressJson.put("street", address.street)
            addressJson.put("zipCode", address.zipCode)
            addressJson.put("city", address.city)
            root.put("address", addressJson)
            root.put("nbMission", etablissementStat.nBMission)
            res.add(root)
        }
        return res
    }

}