package com.gitlab.jeanpascal.thiery.suiviinterim.query.infra.spi

import com.gitlab.jeanpascal.thiery.suiviinterim.business.*
import io.vertx.core.AbstractVerticle
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class MissionDetailRepository : AbstractVerticle() {

    private val logger = LoggerFactory.getLogger(MissionDetailRepository::class.java)

    private val cache = mutableMapOf<InterimMissionID, MissionDetails>()

    override fun start() {
        logger.info("Starting")
        vertx.eventBus()
            .consumer<InterimMissionEvent>(BusAddresses.DOMAIN_EVENT.address) { message ->
                logger.debug("Received event ${message.body()}")
                when (val event = message.body()) {
                    is InterimMissionProposed -> {
                        val missionDetails = MissionDetails(
                            event.mission,
                            MissionState.INCOMMING
                        )
                        cache[event.id] = missionDetails
                        logger.debug("Adding mission $missionDetails")
                    }
                    is InterimMissionStarted -> {
                        cache[event.id]?.let { missionDetails ->
                            cache[event.id] = missionDetails.copy(
                                state = MissionState.INPROGRESS
                            )
                        }
                    }
                    is InterimMissionFinished -> {
                        cache[event.id]?.let { missionDetails ->
                            cache[event.id] = missionDetails.copy(
                                state = MissionState.DONE
                            )
                        }
                    }
                    is InterimMissionRejected -> {
                        cache.remove(event.id)
                    }
                    else -> logger.debug("Drop event $event")
                }
            }
        vertx.eventBus().consumer<String>(BusAddresses.MISSION_DETAIL.address) { message ->
            val id = InterimMissionID(message.body())
            logger.debug("Received request to lookup mission ${id.id}")
            cache[id]?.let { value ->
                logger.debug("Find ! $value")
                message.reply(value)
            } ?: message.fail(404, "Mission id ${id.id} not found.")
            logger.debug("Finish processing ${message.body()}")
        }
    }
}

enum class MissionState {
    INCOMMING,
    INPROGRESS,
    DONE
}

data class MissionDetails(
    val mission: Mission,
    val state: MissionState
)