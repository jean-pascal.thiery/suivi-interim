package com.gitlab.jeanpascal.thiery.suiviinterim.query.infra.api

import io.smallrye.mutiny.Uni
import io.vertx.core.eventbus.ReplyException
import io.vertx.core.json.JsonObject
import io.vertx.mutiny.core.eventbus.EventBus
import io.vertx.mutiny.core.eventbus.Message
import javax.ws.rs.core.Response


fun <M, R> safetyProcessResponseFromEventBus(
    eventBus: EventBus,
    address: String,
    request: M,
    processReply: (reply: Message<R>) -> Response
): Uni<Response> =
    eventBus.request<R>(address, request)
        .map(processReply)
        .onFailure()
        .recoverWithItem { err: Throwable ->
            when (err) {
                is ReplyException -> {
                    val body = JsonObject()
                    body.put("message", err.message)
                    Response.status(err.failureCode()).entity(body).build()
                }
                else -> Response.status(500).entity(err.message).build()
            }
        }

