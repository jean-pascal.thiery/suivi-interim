package com.gitlab.jeanpascal.thiery.suiviinterim.query.infra.api

import com.gitlab.jeanpascal.thiery.suiviinterim.business.BusAddresses
import com.gitlab.jeanpascal.thiery.suiviinterim.query.infra.spi.MissionDetails
import io.smallrye.mutiny.Uni
import io.vertx.core.json.JsonObject
import io.vertx.mutiny.core.eventbus.EventBus
import org.slf4j.LoggerFactory
import javax.enterprise.inject.Default
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/mission")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
class MissionQueryEntrypoint {

    @Inject
    @field: Default
    lateinit var eventBus: EventBus

    private val logger = LoggerFactory.getLogger(MissionQueryEntrypoint::class.java)

    @Path("/{missionId}")
    @GET
    fun getMission(@PathParam("missionId") id: String): Uni<Response> {
        logger.debug("Request for lookup Mission on Id $id")
        return safetyProcessResponseFromEventBus<String, MissionDetails>(
            eventBus,
            BusAddresses.MISSION_DETAIL.address,
            id
        ) { Response.status(Response.Status.OK).entity(toJson(it.body())).build() }
    }

    private fun toJson(missionDetails: MissionDetails): JsonObject {
        val res = JsonObject()
        res.put("etablissement", missionDetails.mission.etablissement.name)
        res.put("state", missionDetails.state.name.lowercase())
        return res
    }

}