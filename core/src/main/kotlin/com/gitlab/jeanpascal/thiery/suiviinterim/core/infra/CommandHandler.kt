package com.gitlab.jeanpascal.thiery.suiviinterim.core.infra

import com.gitlab.jeanpascal.thiery.suiviinterim.business.*
import io.vertx.core.AbstractVerticle
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class CommandHandler(
    @Inject var eventStore: EventStore
) : AbstractVerticle() {

    private val logger = LoggerFactory.getLogger(CommandHandler::class.java)

    override fun start() {
        val cqrsEngine = CqrsEngine(eventStore)
        vertx.eventBus()
            .consumer<Command>(BusAddresses.DOMAIN_COMMAND.address)
            .handler { message ->
                logger.info("Received command :${message.body()}")
                val commandResult = when (val command = message.body()) {
                    is InterimMissionCommand -> {
                        logger.debug("Processing following command :$command")
                        val aggregate = SuiviInterim()
                        cqrsEngine.handleCommand(aggregate, command)
                    }
                    else -> NoopToHandleCommand(command)
                }
                message.reply(commandResult)
            }
    }
}