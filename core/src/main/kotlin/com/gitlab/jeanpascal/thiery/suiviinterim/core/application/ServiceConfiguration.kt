package com.gitlab.jeanpascal.thiery.suiviinterim.core.application

import com.gitlab.jeanpascal.thiery.suiviinterim.business.*
import com.gitlab.jeanpascal.thiery.suiviinterim.core.infra.CommandHandler
import com.gitlab.jeanpascal.thiery.suiviinterim.core.infra.DomainBusCodec
import io.quarkus.runtime.StartupEvent
import io.vertx.core.Vertx
import org.slf4j.LoggerFactory
import javax.enterprise.context.Dependent
import javax.enterprise.event.Observes

@Dependent
class ServiceConfiguration {

    val logger = LoggerFactory.getLogger(ServiceConfiguration::class.java)

    var codecRegistered = false

    fun startCommandHandler(@Observes startupEvent: StartupEvent, vertx: Vertx, commandHandler: CommandHandler) {
        vertx.deployVerticle(commandHandler)
    }

    fun startVertx(@Observes startupEvent: StartupEvent, vertx: Vertx) {
        val eventBus = vertx.eventBus()

        if (!codecRegistered) {
            eventBus.registerDefaultCodec(InterimMissionID::class.java, DomainBusCodec(InterimMissionID::class.java))

            eventBus.registerDefaultCodec(Adresse::class.java, DomainBusCodec(Adresse::class.java))
            eventBus.registerDefaultCodec(Employeur::class.java, DomainBusCodec(Employeur::class.java))
            eventBus.registerDefaultCodec(Etablissement::class.java, DomainBusCodec(Etablissement::class.java))
            eventBus.registerDefaultCodec(PeriodeDeMission::class.java, DomainBusCodec(PeriodeDeMission::class.java))
            eventBus.registerDefaultCodec(Mission::class.java, DomainBusCodec(Mission::class.java))

            eventBus.registerDefaultCodec(ProposerMission::class.java, DomainBusCodec(ProposerMission::class.java))
            eventBus.registerDefaultCodec(RejectMission::class.java, DomainBusCodec(RejectMission::class.java))
            eventBus.registerDefaultCodec(AcceptMission::class.java, DomainBusCodec(AcceptMission::class.java))
            eventBus.registerDefaultCodec(StartMission::class.java, DomainBusCodec(StartMission::class.java))
            eventBus.registerDefaultCodec(FinishMission::class.java, DomainBusCodec(FinishMission::class.java))

            eventBus.registerDefaultCodec(
                InterimMissionProposed::class.java,
                DomainBusCodec(InterimMissionProposed::class.java)
            )
            eventBus.registerDefaultCodec(
                InterimMissionRejected::class.java,
                DomainBusCodec(InterimMissionRejected::class.java)
            )
            eventBus.registerDefaultCodec(
                InterimMissionAccepted::class.java,
                DomainBusCodec(InterimMissionAccepted::class.java)
            )
            eventBus.registerDefaultCodec(
                InterimMissionStarted::class.java,
                DomainBusCodec(InterimMissionStarted::class.java)
            )
            eventBus.registerDefaultCodec(
                InterimMissionFinished::class.java,
                DomainBusCodec(InterimMissionFinished::class.java)
            )

            eventBus.registerDefaultCodec(
                SuccessfullyHandleCommand::class.java,
                DomainBusCodec(SuccessfullyHandleCommand::class.java)
            )
            eventBus.registerDefaultCodec(
                FailedToHandleCommand::class.java,
                DomainBusCodec(FailedToHandleCommand::class.java)
            )
            eventBus.registerDefaultCodec(
                NoopToHandleCommand::class.java,
                DomainBusCodec(NoopToHandleCommand::class.java)
            )
            codecRegistered = true
        }
    }

}