package com.gitlab.jeanpascal.thiery.suiviinterim.core.infra


import com.gitlab.jeanpascal.thiery.suiviinterim.business.*
import javax.annotation.Priority
import javax.enterprise.inject.Alternative

@Alternative
@Priority(1)
class InMemoryEventStore : EventStore {

    private val cache: MutableMap<CacheEntry, List<Event>> = mutableMapOf()

    override fun <C : Command, S : State, E : Event> getEventForAggregate(
        aggregate: Aggregate<C, S, E>,
        id: StreamId
    ): List<E> {
        val cacheEntry = CacheEntry.from(id)
        val res = cache[cacheEntry]
        return if (res == null) {
            listOf()
        } else {
            res as List<E>
        }
    }

    override fun <I : StreamId, C : Command, S : State, E : Event> appendEvents(
        aggregate: Aggregate<C, S, E>,
        streamId: I,
        events: List<E>,
        initialStateVersion: Int
    ): AppendedEventResult {
        val cacheEntry = CacheEntry.from(streamId)
        val storedEvents = cache[cacheEntry]?.toMutableList() ?: mutableListOf()
        storedEvents.addAll(events)
        cache[cacheEntry] = storedEvents.toList()
        val resVersion = initialStateVersion + events.size
        return SuccessfulAppendedEventResult(resVersion)
    }

    data class CacheEntry(val aggregateClass: Class<StreamId>, val id: StreamId) {
        companion object {
            fun from(id: StreamId): CacheEntry = CacheEntry(id.javaClass, id)
        }
    }

}