package com.gitlab.jeanpascal.thiery.suiviinterim.core.application

import com.gitlab.jeanpascal.thiery.suiviinterim.business.EventStore
import com.gitlab.jeanpascal.thiery.suiviinterim.core.infra.EventStorePublisher
import com.gitlab.jeanpascal.thiery.suiviinterim.core.infra.InMemoryEventStore
import io.vertx.core.eventbus.EventBus
import javax.enterprise.context.Dependent
import javax.enterprise.inject.Produces
import javax.inject.Singleton

@Dependent
class DomainConfiguration {

    @Produces
    @Singleton
    fun eventStore(eventBus: EventBus): EventStore {
        val inMemoryEventStore = InMemoryEventStore()
        return EventStorePublisher(
            inMemoryEventStore,
            eventBus
        )
    }

}